#include <TinyGPS++.h>

// The TinyGPS++ object
TinyGPSPlus gps;
unsigned long j = 0;

void setup() {
  Serial.println("GPS echo test");
  Serial.begin(9600);
  Serial1.begin(9600);      // default NMEA GPS baud
}

 //non-blocking code example: read GPS, but don't stop loop   
void loop() {
  j++;
  while (Serial1.available() > 0) {
    gps.encode(Serial1.read());
  }
  
  if (gps.location.isValid() && gps.location.isUpdated()) {
    Serial.println(gps.location.lat(), 6);
    Serial.println(gps.altitude.meters(), 6);
    Serial.println(j);
  }
}