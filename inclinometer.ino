#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Servo.h>

const int LED_PIN = 7;
const int LEFT_ELEVON_PIN = 9;
const int RIGHT_ELEVON_PIN = 10;
const int ESC_PIN = 11;

/* Assign a unique ID to this sensor at the same time */
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);
Servo leftElevon, rightElevon, esc;

const double ACCEL_CALIBRATION_LIMIT = 0.30;

//most recent event
sensors_event_t event;

double initialPitch = 0.0;
double initialRoll = 0.0;

double currentPitch;
double currentRoll;

double correctedPitch;
double correctedRoll;

double targetPitch;
double targetRoll;

void setup(void) {
  Serial.begin(9600);
  
  pinMode(LED_PIN, OUTPUT);  
  leftElevon.attach(LEFT_ELEVON_PIN);
  rightElevon.attach(RIGHT_ELEVON_PIN);
  esc.attach(ESC_PIN);
  
  if(!accel.begin()) abortFlight();
  wait();
  calibratePitch();
  wait();
  calibrateRoll();
  wait();
  preflightControlCheck();
  wait();
  abortFlight();
}

void preflightControlCheck() {
  roll(50);
  delay(1000);
  roll(-50);
  delay(1000);
  roll(0);
  delay(1000);
  pitch(50);
  delay(1000);
  pitch(-50);
  delay(1000);
  pitch(0);
  delay(1000);
}

/*
 * range of settings is +/- 50 (centered = 0)
 * positive values roll to the left (counterclockwise)
 */
void roll(double degree) {
  degree = degree > 50 ? 50 : degree;
  degree = degree < -50 ? -50 : degree;
  leftElevon.write(50+degree);
  rightElevon.write(50-degree);
}

/*
 * range of settings is +/- 50 (centered = 0)
 * positive values pitch upward
 */
void pitch(double degree) {
  degree = degree > 50 ? 50 : degree;
  degree = degree < -50 ? -50 : degree;
  leftElevon.write(degree + 50);
  rightElevon.write(degree + 50);
}

/*
 * this method is the one and only way to
 * update sensor readings.
 */
void update() {
  accel.getEvent(&event);
  currentPitch = event.acceleration.y;
  currentRoll = event.acceleration.x;
  
  // Not valid until calibration is complete!
  correctedPitch = currentPitch - initialPitch;
  correctedRoll = currentRoll - initialRoll;
  
}

/*
 * wait until plane is held nose-up for 1 second
 */
void wait() {
  while (1) {
    update();
    if (event.acceleration.y > 8.0) {
      delay(1000);
      update();
      if (event.acceleration.y > 8.0) {
        break;
      }
    } else {
      flashDelay(100, 1);
    }
  }
  flashDelay(750, 5);
}

void calibratePitch() {
  int i = 0;
  boolean done = false;
  while (i < 10) {
    update();
    double y = event.acceleration.y;
    if (abs(y) < ACCEL_CALIBRATION_LIMIT) {
      i++; 
    } else {
      i = 0;
      flashDelay(100, 1);
    }
    delay(250);
  }
  initialPitch = event.acceleration.y;
  Serial.print("INITIAL PITCH = ");
  Serial.println(initialPitch);
}

void calibrateRoll() {
  int i = 0;
  boolean done = false;
  while (i < 10) {
    update();
    double x = event.acceleration.x;
    if (abs(x) < ACCEL_CALIBRATION_LIMIT) {
      i++; 
    } else {
      i = 0;
      flashDelay(100, 1);
    }
    delay(250);
  }
  initialRoll = event.acceleration.x;
  Serial.print("INITIAL ROLL = ");
  Serial.println(initialRoll);
}

void abortFlight() {
  while (1) {
    digitalWrite(LED_PIN, HIGH);
  }
}

void flashDelay(int frequency, int count) {
  for (int i = 0; i < count; i++) {
    digitalWrite(LED_PIN, HIGH);
    delay(frequency / 2);
    digitalWrite(LED_PIN, LOW);
    delay(frequency / 2);
  }
}

void loop(void) {
  /* Get a new sensor event */
  update();
  double deltaPitch = targetPitch - correctedPitch;
  double deltaRoll = targetRoll - correctedRoll;

  
}